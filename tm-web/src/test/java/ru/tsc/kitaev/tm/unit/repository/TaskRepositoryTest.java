package ru.tsc.kitaev.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kitaev.tm.config.DataBaseConfiguration;
import ru.tsc.kitaev.tm.dto.ProjectDTO;
import ru.tsc.kitaev.tm.dto.TaskDTO;
import ru.tsc.kitaev.tm.marker.WebUnitCategory;
import ru.tsc.kitaev.tm.repository.dto.ProjectDTORepository;
import ru.tsc.kitaev.tm.repository.dto.TaskDTORepository;
import ru.tsc.kitaev.tm.util.UserUtil;

import java.util.List;
import java.util.Optional;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class})
@Category(WebUnitCategory.class)
public class TaskRepositoryTest {

    @NotNull
    private final ProjectDTO project1 = new ProjectDTO("Test Project 1", "Test Project Description 1");

    @NotNull
    private final TaskDTO task1 = new TaskDTO("Test Task 1", "Test Task Description 1");

    @NotNull
    private final TaskDTO task2 = new TaskDTO("Test Task 2", "Test Task Description 2");

    @NotNull
    private final TaskDTO task3 = new TaskDTO("Test Task 3", "Test Task Description 3");

    @NotNull
    @Autowired
    private ProjectDTORepository projectRepository;

    @NotNull
    @Autowired
    private TaskDTORepository taskRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private String userId;

    @Before
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        project1.setUserId(userId);
        task1.setUserId(userId);
        task2.setUserId(userId);
        task3.setUserId(userId);
        task1.setProjectId(project1.getId());
        task2.setProjectId(project1.getId());
        task3.setProjectId(project1.getId());
        projectRepository.save(project1);
        taskRepository.save(task1);
        taskRepository.save(task2);
        taskRepository.save(task3);
    }

    @After
    public void after() {
        taskRepository.deleteAllByUserId(userId);
        projectRepository.delete(project1);
    }

    @Test
    public void findAllByUserIdTest() {
        @Nullable final List<TaskDTO> tasks = taskRepository.findAllByUserId(userId);
        Assert.assertEquals(3, tasks.size());
    }

    @Test
    public void findByUserIdAndIdTest() {
        @Nullable final Optional<TaskDTO> task = taskRepository.findByUserIdAndId(userId, task1.getId());
        Assert.assertEquals(task1.getId(), task.orElse(null).getId());
    }

    @Test
    public void existsByUserIdAndIdTest() {
        Assert.assertFalse(taskRepository.existsByUserIdAndId("", task1.getId()));
        Assert.assertFalse(taskRepository.existsByUserIdAndId(userId, ""));
        Assert.assertTrue(taskRepository.existsByUserIdAndId(userId, task1.getId()));
    }

    @Test
    public void countByUserIdTest() {
        Assert.assertEquals(3, taskRepository.countByUserId(userId));
    }

    @Test
    public void deleteAllByUserIdTest() {
        taskRepository.deleteAllByUserId(userId);
        Assert.assertEquals(0, taskRepository.countByUserId(userId));
    }

    @Test
    public void deleteByUserIdAndIdTest() {
        taskRepository.deleteByUserIdAndId(userId, task3.getId());
        Assert.assertEquals(2, taskRepository.countByUserId(userId));
    }

    @Test
    public void findAllByUserIdAndProjectIdTest() {
        @Nullable final List<TaskDTO> tasks = taskRepository.findAllByUserIdAndProjectId(userId, project1.getId());
        Assert.assertEquals(3, tasks.size());
    }

}
