package ru.tsc.kitaev.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kitaev.tm.config.DataBaseConfiguration;
import ru.tsc.kitaev.tm.dto.ProjectDTO;
import ru.tsc.kitaev.tm.marker.WebUnitCategory;
import ru.tsc.kitaev.tm.repository.dto.ProjectDTORepository;
import ru.tsc.kitaev.tm.util.UserUtil;

import java.util.List;
import java.util.Optional;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class})
@Category(WebUnitCategory.class)
public class ProjectRepositoryTest {

    @NotNull
    private final ProjectDTO project1 = new ProjectDTO("Test Project 1", "Test Project Description 1");

    @NotNull
    private final ProjectDTO project2 = new ProjectDTO("Test Project 2", "Test Project Description 2");

    @NotNull
    private final ProjectDTO project3 = new ProjectDTO("Test Project 3", "Test Project Description 3");

    @NotNull
    @Autowired
    private ProjectDTORepository projectRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private String userId;

    @Before
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        project1.setUserId(userId);
        project2.setUserId(userId);
        project3.setUserId(userId);
        projectRepository.save(project1);
        projectRepository.save(project2);
        projectRepository.save(project3);
    }

    @After
    public void after() {
        projectRepository.deleteAllByUserId(userId);
    }

    @Test
    public void findAllByUserIdTest() {
        @Nullable final List<ProjectDTO> projects = projectRepository.findAllByUserId(userId);
        Assert.assertEquals(3, projects.size());
    }

    @Test
    public void findByUserIdAndIdTest() {
        @Nullable final Optional<ProjectDTO> project = projectRepository.findByUserIdAndId(userId, project1.getId());
        Assert.assertEquals(project1.getId(), project.orElse(null).getId());
    }

    @Test
    public void existsByUserIdAndIdTest() {
        Assert.assertFalse(projectRepository.existsByUserIdAndId("", project1.getId()));
        Assert.assertFalse(projectRepository.existsByUserIdAndId(userId, ""));
        Assert.assertTrue(projectRepository.existsByUserIdAndId(userId, project1.getId()));
    }

    @Test
    public void countByUserIdTest() {
        Assert.assertEquals(3, projectRepository.countByUserId(userId));
    }

    @Test
    public void deleteAllByUserIdTest() {
        projectRepository.deleteAllByUserId(userId);
        Assert.assertEquals(0, projectRepository.countByUserId(userId));
    }

    @Test
    public void deleteByUserIdAndIdTest() {
        projectRepository.deleteByUserIdAndId(userId, project3.getId());
        Assert.assertEquals(2, projectRepository.countByUserId(userId));
    }

}
