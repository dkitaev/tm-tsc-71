package ru.tsc.kitaev.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public class MessageDTO {

    private String value;

    public MessageDTO(@NotNull final String value) {
        this.value = value;
    }

}
