package ru.tsc.kitaev.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class TerminalConst {

    @NotNull
    public static final String HELP = "help";

    @NotNull
    public static final String EXIT = "exit";

}
