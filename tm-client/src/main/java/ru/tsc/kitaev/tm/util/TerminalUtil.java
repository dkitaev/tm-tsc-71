package ru.tsc.kitaev.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.exception.AbstractException;
import ru.tsc.kitaev.tm.exception.system.IndexIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() throws AbstractException {
        @NotNull final String value = SCANNER.nextLine();
        try {
            return Integer.parseInt(value);
        } catch (@NotNull final RuntimeException e) {
            throw new IndexIncorrectException(value);
        }
    }

}
