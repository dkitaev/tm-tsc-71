package ru.tsc.kitaev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.event.ConsoleEvent;
import ru.tsc.kitaev.tm.listener.AbstractListener;
import ru.tsc.kitaev.tm.util.TerminalUtil;

@Component
public class UserUpdateProfileListener extends AbstractListener {

    @NotNull
    @Override
    public String command() {
        return "update-user";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "update user profile...";
    }

    @Override
    @EventListener(condition = "@userUpdateProfileListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[UPDATE PROFILE]");
        System.out.println("ENTER FIRST NAME:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        userEndpoint.updateUser(sessionService.getSession(), firstName, lastName, middleName);
        System.out.println("[OK]");
    }

}
