package ru.tsc.kitaev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.dto.ProjectDTO;

import java.util.List;

public interface IProjectDTOService extends IOwnerDTOService<ProjectDTO> {

    void create(@Nullable final String userId, @Nullable final String name);

    @NotNull
    ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    );

    @NotNull
    ProjectDTO findByName(@Nullable final String userId, @Nullable final String name);

    void removeByName(@Nullable final String userId, @Nullable final String name);

    void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @NotNull final String description
    );

    void updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @NotNull String description
    );

    void startById(@Nullable final String userId, @Nullable final String id);

    void startByIndex(@Nullable final String userId, @Nullable final Integer index);

    void startByName(@Nullable final String userId, @Nullable final String name);

    void finishById(@Nullable final String userId, @Nullable final String id);

    void finishByIndex(@Nullable final String userId, @Nullable final Integer index);

    void finishByName(@Nullable final String userId, @Nullable final String name);

    void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    );

    void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    );

    void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    );

}
